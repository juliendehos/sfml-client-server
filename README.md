# Client-server example  
[![Build status](https://gitlab.com/juliendehos/sfml-client-server/badges/master/build.svg)](https://gitlab.com/juliendehos/sfml-client-server/pipelines) 

## Build

- install [sfml](https://www.sfml-dev.org/)

```
mkdir build
cd build
cmake ..
make 
```

## Run

- 1 server + 2 clients

```
$ ./server.out 
begin server
sending: hello from server
sending: hello from server
received: hello from client 140331221037760
received: hello from client 140315187724992
received: hello from client 140331221037760
received: hello from client 140331221037760
received: hello from client 140315187724992
received: hello from client 140315187724992
received: hello from client 140331221037760
received: hello from client 140315187724992
received: hello from client 140331221037760
received: hello from client 140315187724992
```

```
$ ./client.out 
begin client
received: hello from server
sending: hello from client 140315187724992
sending: hello from client 140315187724992
sending: hello from client 140315187724992
sending: hello from client 140315187724992
sending: hello from client 140315187724992
end client
```

```
$ ./client.out 
begin client
received: hello from server
sending: hello from client 140331221037760
sending: hello from client 140331221037760
sending: hello from client 140331221037760
sending: hello from client 140331221037760
sending: hello from client 140331221037760
end client
```

