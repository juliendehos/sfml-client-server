
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <thread>
#include <SFML/Network.hpp>

int main()
{
    std::cout << "begin server" << std::endl;

    // list of sockets and selector
    using uptrSocket_t = std::unique_ptr<sf::TcpSocket>;
    std::list<uptrSocket_t> sockets;
    sf::SocketSelector selector;

    // init listener
    sf::TcpListener listener;
    if (listener.listen(3000) != sf::Socket::Done)
        exit(-1);
    selector.add(listener);

    // format welcome message
    std::string stringToClient = "hello from server";
    sf::Packet packetToClient;
    packetToClient << stringToClient;

    // main server loop
    while (true)
    {
        // wait for data
        if (selector.wait())
        {
            // data from the listener (new client)
            if (selector.isReady(listener))
            {
                // accept a new client
                uptrSocket_t uptrSocket = std::make_unique<sf::TcpSocket>();
                if (listener.accept(*uptrSocket) != sf::Socket::Done)
                    continue;

                // send welcome message to the new client
                std::cout << "sending: " << stringToClient << std::endl;
                if (uptrSocket->send(packetToClient) != sf::Socket::Done)
                    continue;

                // register the new client
                selector.add(*uptrSocket);
                sockets.push_front(std::move(uptrSocket));
            }
            else
            {
                // data from a registered client
                for (uptrSocket_t & clientSocket: sockets)
                {
                    // we found the client
                    if (selector.isReady(*clientSocket))
                    {
                        // receive data from the client
                        sf::Packet packetFromClient;
                        if (clientSocket->receive(packetFromClient) 
                                != sf::Socket::Done)
                            continue;
                        std::cout << "received:";
                        std::string m;
                        while (packetFromClient >> m) { std::cout << " " << m; }
                        std::cout << std::endl;
                    }
                }
            }
        }        
    }

    std::cout << "end server" << std::endl;

    return 0;
}

