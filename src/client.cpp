
#include <chrono>
#include <iostream>
#include <random>
#include <sstream>
#include <thread>
#include <SFML/Network.hpp>

int main()
{

    std::cout << "begin client" << std::endl;

    // connect to server
    sf::TcpSocket socketToServer;
    if (socketToServer.connect("127.0.0.1", 3000) != sf::Socket::Done)
        exit(-1);

    // get a message from the server
    sf::Packet packetFromServer;    
    if (socketToServer.receive(packetFromServer) != sf::Socket::Done)
        exit(-1);
    std::cout << "received:";
    std::string m;
    while (packetFromServer >> m) { std::cout << " " << m; }
    std::cout << std::endl;

    // wait some random time and send a message, 5 times
    std::mt19937 engine(std::random_device{}());
    std::uniform_int_distribution<int> dist(1, 5);
    std::ostringstream oss;
    oss << "hello from client " << std::this_thread::get_id();
    sf::Packet packetToServer;
    packetToServer << oss.str();
    for (int i=0; i<5; i++)
    {
        int t = dist(engine);
        std::this_thread::sleep_for(std::chrono::seconds(t));
        std::cout << "sending: " << oss.str() << std::endl;
        if (socketToServer.send(packetToServer) != sf::Socket::Done)
            exit(-1);
    }

    std::cout << "end client" << std::endl;

    return 0;
}


